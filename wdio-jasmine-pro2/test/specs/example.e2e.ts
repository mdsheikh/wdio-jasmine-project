import {
  TC001_RET_CAL_NO_SSN_INC_TD001,
  TC002_RET_CAL_YES_SSN_INC_SINGLE_TD002,
  TC003_RET_CAL_YES_SSN_INC_MARRIED_TD003,
} from "../pageobjects/data/test-data.file-01";
import HomePage from "../pageobjects/home.page";
import LoginPage from "../pageobjects/login.page";

//const DataJson = require("../pageobjects/data/test-data-json.json");

describe("My Login application", () => {
  beforeEach(async () => {
    const username = "user1";
    await LoginPage.appLogin("actual-url/");
    browser.pause(1000);
  });

  it("TC0001- Calculate_Retirement_with_No_SSI", async () => {
    HomePage.validateHeader();
    HomePage.userInputWithNoSSI(TC001_RET_CAL_NO_SSN_INC_TD001);
    HomePage.calculateSavings();
    HomePage.validateHeader();
    HomePage.validateClaculatedResult(TC001_RET_CAL_NO_SSN_INC_TD001);
  });
  it("TC0001- Calculate_Retirement_with_Yes_Single_SSI", async () => {
    HomePage.validateHeader();
    HomePage.userInputWithYesSingle(TC002_RET_CAL_YES_SSN_INC_SINGLE_TD002);
    HomePage.calculateSavings();
    HomePage.validateClaculatedResult(TC002_RET_CAL_YES_SSN_INC_SINGLE_TD002);
  });

  it("TC0001- Calculate_Retirement_with_Yes_married_SSI", async () => {
    HomePage.validateHeader();
    HomePage.userInputWithYesMarried(TC003_RET_CAL_YES_SSN_INC_MARRIED_TD003);
    HomePage.calculateSavings();
    HomePage.validateClaculatedResult(TC003_RET_CAL_YES_SSN_INC_MARRIED_TD003);
  });
});
