import Page from './page';

/**
 * sub page containing specific selectors and methods for a specific page
 */
class LoginPage extends Page {
  public get inputUsername() {
    return $("#username");
  }

  public get inputPassword() {
    return $("#password");
  }

  public get btnSubmit() {
    return $('button[type="submit"]');
  }

  /**
   * a method to encapsule automation code to interact with the page
   * e.g. to login using username and password
   */
  async open(path: string): Promise<void> {
    await super.open(path);
    await browser.pause(1000);
    await browser.maximizeWindow();
    await browser.pause(1000);
  }
  async setUserName() {
    const userName = browser.$("#user-signin-usrname");
    await userName.waitForDisplayed({
      timeout: 15000,
      timeoutMsg: "username visible timeout",
    });
    await userName.clearValue();
    await userName.setValue("username");
    await browser.pause(1000);
  }

  async setPassword() {
    const userName = browser.$("#user-signin-password");
    await userName.waitForDisplayed({
      timeout: 15000,
      timeoutMsg: "username visible timeout",
    });
    await userName.clearValue();
    await userName.setValue("userpass");
    await browser.pause(1000);
  }

  async submitLogin() {
    const appSigninSubmit = await browser.$("#app-signin-submit");
    await appSigninSubmit.waitForDisplayed({
      timeout: 5000,
      timeoutMsg: "submit button visibe timeout",
    });
    await appSigninSubmit.click();
    await browser.pause(1000);
    await super.waitForPageLoad(120);
    const loggedInPage = browser.$("span=Main Menu");
    await loggedInPage.waitForDisplayed({
      timeout: 5000,
      timeoutMsg: "Main menu visibe timeout",
    });
  }

  async appLogin(pathURL: string = "https://syz.com/") {
    await this.open(pathURL);
    await browser.pause(1000);
    const loggedInPageElementButton = await $("button=Order Entry");
    let alreadyLoggedIn = await loggedInPageElementButton.isExisting();
    if (!alreadyLoggedIn) {
      await this.setUserName();
      await this.setPassword();
      await this.submitLogin();
    } else {
      console.log("***** alread login **********");
    }
  }
}

export default new LoginPage();
