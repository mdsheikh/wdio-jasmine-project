export interface CustomerData {
  currentAge?: number;
  retirementAge?: number;
  currentAnnualIncome?: any;
  spouseAnnualIncome?: any;
  currentRetirementSavings?: any;
  currentRetirementContribution?: any;
  annualRetContribuIncrease?: any;
  ssnIncome?: any;
  reltionshipStatus?: any;
  ssnOveride?: any;
  addlIncome?: any;
  numberOfYears?: number;
  postRetIncomeIncrease?: any;
  finalAnnualIncomeDesired?: any;
  preRetInvestReturn?: any;
  postRetInvestReturn?: any;
  header?: any;
}
