import { CustomerData } from './test-data-base';

export const TC001_RET_CAL_NO_SSN_INC_TD001: CustomerData = {
  currentAge: 40,
  retirementAge: 68,
  currentAnnualIncome: 100000,
  spouseAnnualIncome: 75000,
  currentRetirementSavings: 500000,
  currentRetirementContribution: 0.1,
  annualRetContribuIncrease: 0.025,
};
