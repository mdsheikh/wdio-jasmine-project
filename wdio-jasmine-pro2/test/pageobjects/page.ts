/**
 * main page object containing all methods, selectors and functionality
 * that is shared across all page objects
 */
export default class Page {
  /**
   * Opens a sub page of the page
   * @param path path of the sub page (e.g. /path/to/page.html)
   */
  async open(path: string) {
    browser.url(`${path}`);
    this.waitForPageLoad();
  }

  async waitForPageLoad(timeInSeconds: number = 120) {
    await browser.waitUntil(
      () => browser.execute(() => document.readyState === "complete"),
      {
        timeout: timeInSeconds * 1000, //60 seconds
        timeoutMsg: "Message on failure",
      }
    );
  }

  async waitForSpinnerToComplete() {
    const alertList = await browser.$$("div.alert");
    const alertCount = await alertList.length;
    if (alertCount > 0) {
      alertList.forEach(async (eachList) => {
        const btnClose = await eachList.$("button.close");
        await btnClose.waitForDisplayed({
          timeout: 15000,
          timeoutMsg: "button visible timeout",
        });
        await btnClose.click();
        await browser.pause(1000);
      });
    }
  }
}
