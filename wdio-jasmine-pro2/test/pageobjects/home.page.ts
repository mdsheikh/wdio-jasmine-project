import { CustomerData } from './data/test-data-base';
import Page from './page';

class HomePage extends Page {
  get getCurrentAge() {
    return $("#current-age");
  }
  get getRetirementAge() {
    return $("#retirement-age");
  }
  get getCurrentIncome() {
    return $("#current-income");
  }

  get getSpouseIncome() {
    return $("#spouse-income");
  }

  get getCurrentSavings() {
    return $("//input[@id = 'current-total-savings']");
  }
  get getCurrentAnnualSavings() {
    return $("//input[@id = 'current-annual-savings']");
  }
  get getSavingRateIncrease() {
    return $("//input[@id = 'savings-increase-rate']");
  }
  get ssnBenefitsNo() {
    return $("//input[@id = 'no-social-benefits']");
  }
  get ssnBenefitsYes() {
    return $("//input[@id = 'yes-social-benefits']");
  }

  public get header() {
    return $("h1");
  }

  async calculateSavings() {
    const button = await $(
      "//*[@id='retirement-form']/div[4]/div[2]/div[1]/button"
    );
    await button.waitForExist({
      timeout: 5000,
      timeoutMsg: "calcualte button timeout",
    });
    await button.click();
  }

  public async userInputWithNoSSI(testData: CustomerData) {
    (await this.getCurrentAge).setValue(testData.currentAge);
    (await this.getRetirementAge).setValue(testData.retirementAge);
    (await this.getCurrentIncome).setValue(testData.currentAnnualIncome);
    (await this.getSpouseIncome).setValue(testData.spouseAnnualIncome);
    (await this.getCurrentSavings).setValue(testData.currentRetirementSavings);
    (await this.getCurrentAnnualSavings).setValue(
      testData.currentRetirementContribution
    );
    (await this.getSavingRateIncrease).setValue(testData.postRetIncomeIncrease);
    (await this.ssnBenefitsNo).click();
  }
  public async userInputWithYesSingle(testData: CustomerData) {
    (await this.getCurrentAge).setValue(testData.currentAge);
    (await this.getRetirementAge).setValue(testData.retirementAge);
    (await this.getCurrentIncome).setValue(testData.currentAnnualIncome);
  }
  public async userInputWithYesMarried(testData: CustomerData) {
    (await this.getCurrentAge).setValue(testData.currentAge);
    (await this.getRetirementAge).setValue(testData.retirementAge);
    (await this.getCurrentIncome).setValue(testData.currentAnnualIncome);
  }
  public async validateClaculatedResult(testData: CustomerData) {
    const result = await $("h3");
    await result.waitForExist({
      timeout: 1500,
      timeoutMsg: "h3 display timepout",
    });
    const resultText = await result.getText();
    expect(resultText).toHaveValue("Result");
    const age = await $('//*[@id="result-message"]');
    await age.waitForExist({
      timeout: 15000,
      timeoutMsg: "result mesage visible timeout",
    });
    const ageText = await age.getText();
    expect(ageText).toHaveValueContaining(
      `In order to retire by ${testData.retirementAge}`
    );
  }

  public async validateHeader() {
    const header = await $("header");
    await header.waitForExist({
      timeout: 1400,
      timeoutMsg: "header visible timeout",
    });
    const headerText = await header.getText();
    expect(headerText).toBe("Pre-retirement calculator");
  }
}
export default new HomePage();
